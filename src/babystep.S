/* Well, here we go */
.global _start
_start:
    jmp _start/* This takes up 2 bytes when assembled */

/* Using rept, endr and byte instead of times and db like the nasm assembler uses */
.rept 508/* Fill up the boot sector with zeroes */
.byte 0
.endr
/* The last two bytes are 0x55AA to indicate a boot sector */
.byte 0x55
.byte 0xAA
