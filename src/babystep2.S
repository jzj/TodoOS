/* Cleaner version of babystep */
.section .text.init
.global __start /* Linker script expects __start as entry function*/
__start:
    jmp __start/* Die forever. */
    
/* Boot sector signature is now handled by linker script */
