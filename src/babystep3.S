/* Hello world attempt */
.section .text.init/* Code section of linker script */
.global __start /* Linker script expects __start as entry function */
__start:/* Actual __start function definition */
    /* AT&T/GNU(GAS) x86 assembly syntax puts the destination register as the second operand
     * (gross) and prefixes immediates with $ and registers with %
     * Holy crap I miss RISC-V assembly */

    /* For the bios interrupt call "int 0x10," the bios is passed an additional parameter
     * in the high byte of the x86 a register (%ah). 0x0E means Write Character in TTY Mode
     * https://en.wikipedia.org/wiki/BIOS_interrupt_call#Interrupt_table */
    mov $0x0E, %ah
    
    /* The character to write is placed in the low byte of the a register, %al */
    mov $'H', %al
    
    /* "int 0x10" is the Video Services interrupt request. Function specified by %ah
     * https://en.wikipedia.org/wiki/BIOS_interrupt_call#Interrupt_table
     */
    int $0x10
    
    /* Yes I know this should be a loop. Baby steps */
    mov $0x0E, %ah
    mov $'e', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'l', %al
    int $0x10
    int $0x10/* Two ls */
    
    mov $0x0E, %ah
    mov $'o', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $' ', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'W', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'o', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'r', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'l', %al
    int $0x10

    mov $0x0E, %ah
    mov $'d', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $' ', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'f', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'r', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'o', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'m', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $' ', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'T', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'o', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'d', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'o', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'O', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'S', %al
    int $0x10
    
    mov $0x0E, %ah
    mov $'!', %al
    int $0x10
    
hang:
    jmp hang/* Die forever. */
    
/* Boot sector signature is now handled by linker script */
